import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import model.Artist;
import model.Genre;
import model.Song;

import java.io.File;
import java.util.List;

public class Controller {

    @FXML
    private ObservableList<String> genre = FXCollections.observableArrayList();

    @FXML
    private ObservableList<String> artist = FXCollections.observableArrayList();

    @FXML
    private ComboBox<String> comboGenre = new ComboBox<String>();

    @FXML
    private ComboBox<String> comboArtist = new ComboBox<String>();

    @FXML
    private TableView<Song> table = new TableView<>();

    @FXML
    private TableColumn<Song, String> columnSong;

    @FXML
    private TableColumn<Song, String> columnArtist;

    @FXML
    private TableColumn<Song, String> columnGenre;

    @FXML
    private TableColumn<Song, String> columnAlbum;

    @FXML
    private TableColumn<Song, String> columnYear;

    @FXML
    private TableColumn<Song, String> columnTime;

    MediaPlayer player;

    public void initialize() {
        columnSong.setCellValueFactory(new PropertyValueFactory<>("name"));
        columnArtist.setCellValueFactory(new PropertyValueFactory<>("artist"));
        columnGenre.setCellValueFactory(new PropertyValueFactory<>("genre"));
        columnAlbum.setCellValueFactory(new PropertyValueFactory<>("album"));
        columnYear.setCellValueFactory(new PropertyValueFactory<>("year"));
        columnTime.setCellValueFactory(new PropertyValueFactory<>("time"));

        table.setRowFactory(tv -> {
            TableRow<Song> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    String uriString = new File(table.getSelectionModel().getSelectedItem().getLink().replaceAll("\\?", "")).toURI().toString();
                    //player.stop();
                    //System.out.println(table.getSelectionModel().getSelectedItem().getLink());
                    try {
                        stop();
                        player = null;
                    } catch (Exception e) {
                    }
                    player = new MediaPlayer(new Media(uriString));
                    play();
                }
            });
            return row;
        });
    }

    @FXML
    private void open() {

//        final DirectoryChooser directoryChooser = new DirectoryChooser();
//
//        File dir = directoryChooser.showDialog(null);
//        String str ="\\\\";
//        SongDao.setLink(String.valueOf(dir).replaceAll(str,"/"));

        List<Genre> genres = SongsGetters.getGenre();
        for (Genre genre : genres) {

            genre.add(genre.getGenre());
        }
        comboGenre.setItems(genre);
    }

    @FXML
    private void actionComboGenre() {
        artist.clear();

        List<Artist> artists = SongsGetters.getArtistsByGenre(comboGenre.getValue());
        for (Artist artist : artists) {
            this.artist.add(artist.getArtists());
        }

        artist.addAll();
        comboArtist.setItems(artist);
    }

    @FXML
    private void actionComboArtist() {
        List songs = SongsGetters.getSongByArtist(comboArtist.getValue());
        ObservableList<Song> songData = FXCollections.observableArrayList();
        songData.addAll(songs);
        table.setItems(songData);
    }


    @FXML
    private void play() {
        player.play();
        System.out.println();
    }

    @FXML
    private void stop() {
        player.pause();
    }

}
