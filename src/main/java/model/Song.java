package model;

public class Song {
    private String name;

    private String genre;

    private String artist;

    private String link;

    private String album;

    private String year;

    private String time;

    public Song(String name, String genre, String artist, String link, String album, String year, String time) {
        this.name = name;
        this.genre = genre;
        this.artist = artist;
        this.link = link;
        this.time = time;
        this.album = album;
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "Song{" +
                "name='" + name + '\'' +
                ", genre='" + genre + '\'' +
                ", artist='" + artist + '\'' +
                ", link='" + link + '\'' +
                ", album='" + album + '\'' +
                ", year='" + year + '\'' +
                ", time='" + time + '\'' +
                '}';
    }
}
