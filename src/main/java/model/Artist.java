package model;

public class Artist {
    private String genre;
    private String name;

    public Artist(String genre, String artist) {
        this.genre = genre;
        this.name = artist;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getArtists() {
        return name;
    }

    public void setArtists(String artist) {
        this.name = artist;
    }

    @Override
    public String toString() {
        return "Artist{" +
                "genre='" + genre + '\'' +
                ", artist='" + name + '\'' +
                '}';
    }
}
